FROM ubuntu:14.04
MAINTAINER Sean Watson

RUN apt-get update
RUN apt-get install -y software-properties-common 
RUN apt-add-repository -y ppa:mosquitto-dev/mosquitto-ppa
RUN apt-get update \
 && apt-get install -y -q --no-install-recommends \
    libmosquitto-dev \
    libcurl4-gnutls-dev \
    libconfig8-dev \
    git \
    build-essential \
 && apt-get clean \
 && rm -r /var/lib/apt/lists/*

WORKDIR /
RUN git clone https://github.com/owntracks/recorder.git
WORKDIR /recorder/

COPY config.mk /recorder/
COPY ot-recorder.config /recorder/
RUN mkdir htdocs
RUN mkdir store
RUN cat /etc/ssl/certs/* > ca.pem

RUN make
RUN make install
RUN ot-recorder --initialize


EXPOSE 8083
VOLUME ["/recoder/store"]

CMD ["/usr/local/sbin/ot-recorder"]
